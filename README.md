# 0x_ini
Very small single-header callback-based ini parsing library.

supports reading, and writing.

additionally includes a list_t type (singly linked list), ini_t (doubly linked list) as these are used in some functionality.
Includes facilities for stripping leading, trailing whitespaces with a wrapper around these for stripping both.



## Example program:

    #include "include/0x_ini.h"
    #include "include/0x_containers.h"
    #include <stdlib.h>
    #include <assert.h>
    
    /* 
     * doubly linked list type domain-specialized for ini files, 
     * provided with the library in "include/0x_containers.h".
     */
    ini_t *ini; 
  
    /*
     * example callback
     */

    uint32 callback(char *category, char *variable, char *value)
    {
        /* 
         * insert an entry into the ini file, here we strip all blankspaces from the variable before entering it
         * because "variable" is easier to use as a key than "variable    "
         * optionally one might want to strip the value as well.
         * there's also strip_leading, strip_trailing, in addition to strip_blank, see "include/0x_ini.h"
         */
        ini = ini_insert(ini, category, strip_blank(variable), value);
        return 0;
    }

    /*
     * This example program expects one or more ini files as command-line parameters
     */
    int main(int argc, char **argv)
    {
        uint32 err = 0x0;
        assert(!(argc < 2) && "file NULL or invalid");
        
        int i = 1;
        while(i < argc)
        {
            ini = ini_create();
            FILE* fp = fopen(argv[i], "r");
            assert(fp && "can't open file");
            
            err = parse_ini(fp, callback);
            if(err)
                printf("failed to parse ini due to error 0x%08lX\n", err);
            fclose(fp);
            
            err = dump_ini(argv[i], ini);
            if(err)
                printf("failed to dump ini due to error 0x%08lX\n",  err);
            
            ini = ini_free(ini);
            i++;
        }
        return EXIT_SUCCESS;
    }
    
    

If you do not want to preserve comments edit the switch statement in parse_ini() to include:

    case ';': case '#':
        while(c= getc(f) != '\n')
           (void)0;
        break;
this will strip comments from the ini file, this is only problematic if you want to write the file out again containing all the comments.

Additionally we preserve blank lines, this shouldn't be a major concern - for a typical ini file this adds maybe 20-30 bytes overhead and aids tremendously with reproducing it.

any ini section as variable = '' will be clobbered down to a flat switch, i.e `bFileRead = ''` -> `bFileRead`

### does not support:
1. multi-line entries (strictly entries containing '\\n'), these will not be supported - this s not within the scope of what I'm doing
2. entries containing '\[' or '\]', this is also not within the scope of support I need.

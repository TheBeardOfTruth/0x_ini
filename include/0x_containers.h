#ifndef H__CONTAINERS_H
#define H__CONTAINERS_H

typedef struct list_t list_t;
struct list_t
{
    void *entry;
    list_t *next;
};

list_t *list_create()
{
    list_t *tmp = malloc(sizeof(list_t));
    if(!tmp)
        return NULL;
    
    memset(tmp, 0, sizeof(list_t));
    return tmp;
}

list_t *list_insert(list_t *list, void *val)
{
    if(list == NULL)
        return NULL;
    list_t *tmp = list;
    
    while(tmp->next != NULL)
        tmp = tmp->next;
    
    if(tmp->entry != NULL)
    {
        tmp->next = list_create();
        tmp = tmp->next; 
    }
    tmp->entry = malloc(strlen((char*)val)+1);
    strcpy(tmp->entry, (char*)val);

    return tmp;
}

list_t *list_free(list_t *head)
{
    while(head != NULL)
    {
        list_t *tmp = head;
        head  = head->next;
        free(tmp->entry);
        free(tmp);
    }
    return head;
}

typedef struct ini_t ini_t;
struct ini_t
{
    char   *category;
    list_t *variable, *value, 
           *varHead,  *valHead;
    
    ini_t  *next, *prev;
};

ini_t *ini_create()
{
    ini_t *tmp = malloc(sizeof(ini_t));
    if(tmp == NULL)
        return NULL;
    
    memset(tmp, 0, sizeof(ini_t));
    tmp->variable = list_create();
    tmp->value    = list_create();
    tmp->varHead  = tmp->variable;
    tmp->valHead  = tmp->value;
    return tmp;
}

ini_t *ini_free(ini_t *head)
{
    while(head->prev != NULL)
        head=head->prev;
    while(head != NULL)
    {
        ini_t *tmp = head;
        head=head->next;
        list_free(tmp->varHead);
        list_free(tmp->valHead);
        free(tmp->category);
        free(tmp);
        
    }
    return head;
}

ini_t *ini_insert(ini_t *ini, char *cats, char *var, char *val)
{
    if(ini == NULL)
        return NULL;
    ini_t *tmp = ini;
        
    if(tmp->next != NULL)
        tmp=tmp->next;
    
    if(tmp->category == NULL)
    {
        tmp->category = malloc(strlen(cats)+1);
        strcpy(tmp->category, cats);
    }
    else if(strcmp(tmp->category, cats) != 0)
    {
        tmp->next = ini_create();
        if(tmp->next == NULL)
            return NULL;
        tmp->next->prev = tmp;
        tmp = tmp->next;
        tmp->category = malloc(strlen(cats)+1);
        strcpy(tmp->category, cats);
    }
    
    tmp->variable = list_insert(tmp->variable, var);
    tmp->value    = list_insert(tmp->value, val);
    
    return tmp;
}

#endif /*H__CONTAINERS_H*/
